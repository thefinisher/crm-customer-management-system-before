import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/dashboard'
        },
        {
            path: '/',
            component: () => import(/* webpackChunkName: "home" */ '../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    path: '/dashboard',
                    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Dashboard.vue'),
                    meta: { title: '系统首页' }
                },
                {
                    path: '/Dashboardtwo',
                    component: () => import(/* webpackChunkName: "Dashboardtwo" */ '../components/page/Dashboardtwo.vue'),
                    meta: { title: '系统首页' }
                },
                {
                    path: '/icon',
                    component: () => import(/* webpackChunkName: "icon" */ '../components/page/Icon.vue'),
                    meta: { title: '自定义图标' }
                },
                {
                    path: '/table',
                    component: () => import(/* webpackChunkName: "table" */ '../components/page/BaseTable.vue'),
                    meta: { title: '基础表格' }
                },
                {
                    path: '/tabs',
                    component: () => import(/* webpackChunkName: "tabs" */ '../components/page/Tabs.vue'),
                    meta: { title: 'tab选项卡' }
                },
                {
                    path: '/form',
                    component: () => import(/* webpackChunkName: "form" */ '../components/page/BaseForm.vue'),
                    meta: { title: '基本表单' }
                },
                {
                    // 富文本编辑器组件
                    path: '/editor',
                    component: () => import(/* webpackChunkName: "editor" */ '../components/page/VueEditor.vue'),
                    meta: { title: '富文本编辑器' }
                },
                {
                    // markdown组件
                    path: '/markdown',
                    component: () => import(/* webpackChunkName: "markdown" */ '../components/page/Markdown.vue'),
                    meta: { title: 'markdown编辑器' }
                },
                {
                    // 图片上传组件
                    path: '/upload',
                    component: () => import(/* webpackChunkName: "upload" */ '../components/page/Upload.vue'),
                    meta: { title: '文件上传' }
                },
                {
                    // vue-schart组件
                    path: '/charts',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/page/BaseCharts.vue'),
                    meta: { title: 'schart图表' }
                },
                {
                    // 拖拽列表组件
                    path: '/drag',
                    component: () => import(/* webpackChunkName: "drag" */ '../components/page/DragList.vue'),
                    meta: { title: '拖拽列表' }
                },
                {
                    // 拖拽Dialog组件
                    path: '/dialog',
                    component: () => import(/* webpackChunkName: "dragdialog" */ '../components/page/DragDialog.vue'),
                    meta: { title: '拖拽弹框' }
                },
                {
                    // 国际化组件
                    path: '/i18n',
                    component: () => import(/* webpackChunkName: "i18n" */ '../components/page/I18n.vue'),
                    meta: { title: '国际化' }
                },
                {
                    // 权限页面
                    path: '/permission',
                    component: () => import(/* webpackChunkName: "permission" */ '../components/page/Permission.vue'),
                    meta: { title: '权限测试', permission: true }
                },
                {
                    path: '/404',
                    component: () => import(/* webpackChunkName: "404" */ '../components/page/404.vue'),
                    meta: { title: '404' }
                },
                {
                    path: '/403',
                    component: () => import(/* webpackChunkName: "403" */ '../components/page/403.vue'),
                    meta: { title: '403' }
                },
                {
                    path: '/donate',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/page/Donate.vue'),
                    meta: { title: '支持作者' }
                },
                {
                    path: '/MassageAdd',
                    component: () => import(/* webpackChunkName: "MassageAdd" */ '../components/pageAdd/MassageAdd.vue'),
                    meta: { title: '创建信息' }
                },
                {
                    path: '/MassageEdit',
                    component: () => import(/* webpackChunkName: "MassageEdit" */ '../components/pageAdd/MassageEdit.vue'),
                    meta: { title: '编辑信息' }
                },
                {
                    path: '/Docking',
                    component: () => import(/* webpackChunkName: "Docking" */ '../components/pageAdd/Docking.vue'),
                    meta: { title: '系统对接管理' }
                },
                {
                    path: '/Allocation',
                    component: () => import(/* webpackChunkName: "Allocation" */ '../components/pageAdd/Allocation.vue'),
                    meta: { title: '系统配置' }
                },
                {
                    path: '/details',
                    component: () => import(/* webpackChunkName: "details" */ '../components/pageAdd/details.vue'),
                    meta: { title: '信息详情' }
                },
                {
                    path: '/search',
                    component: () => import(/* webpackChunkName: "search" */ '../components/pageAdd/search.vue'),
                    meta: { title: '搜索信息' }
                },
                {
                    path: '/contactsAdd',
                    component: () => import(/* webpackChunkName: "contactsAdd" */ '../components/pageAdd/contactsAdd.vue'),
                    meta: { title: '创建联系人' }
                },
                {
                    path: '/contactslist',
                    component: () => import(/* webpackChunkName: "contactslist" */ '../components/pageAdd/contactslist.vue'),
                    meta: { title: '联系人列表' }
                },
                {
                    path: '/contactsEidt',
                    component: () => import(/* webpackChunkName: "contactsEidt" */ '../components/pageAdd/contactsEidt.vue'),
                    meta: { title: '联系人列表' }
                },
                {
                    path: '/contactsXQ',
                    component: () => import(/* webpackChunkName: "contactsXQ" */ '../components/pageAdd/contactsXQ.vue'),
                    meta: { title: '联系人列表' }
                },
                {
                    path: '/constactsSerch',
                    component: () => import(/* webpackChunkName: "constactsSerch" */ '../components/pageAdd/constactsSerch.vue'),
                    meta: { title: '联系人搜索' }
                },
                {
                    path: '/businessAdd',
                    component: () => import(/* webpackChunkName: "businessAdd" */ '../components/pageAdd/businessAdd.vue'),
                    meta: { title: '创建商机' }
                },
                {
                    path: '/businesslist',
                    component: () => import(/* webpackChunkName: "businesslist" */ '../components/pageAdd/businesslist.vue'),
                    meta: { title: '商机列表' }
                },
                {
                    path: '/businessEidt',
                    component: () => import(/* webpackChunkName: "businessEidt" */ '../components/pageAdd/businessEidt.vue'),
                    meta: { title: '编辑商机' }
                },
                {
                    path: '/businessXQ',
                    component: () => import(/* webpackChunkName: "businessXQ" */ '../components/pageAdd/businessXQ.vue'),
                    meta: { title: '商机详情' }
                },
                {
                    path: '/businessSearch',
                    component: () => import(/* webpackChunkName: "businessSearch" */ '../components/pageAdd/businessSearch.vue'),
                    meta: { title: '商机搜索' }
                },
                {
                    path: '/contractlist',
                    component: () => import(/* webpackChunkName: "contractlist" */ '../components/pageAdd/contractlist.vue'),
                    meta: { title: '合同列表' }
                },
                {
                    path: '/contractSearch',
                    component: () => import(/* webpackChunkName: "contractSearch" */ '../components/pageAdd/contractSearch.vue'),
                    meta: { title: '合同搜索' }
                },
                {
                    path: '/contractXQ',
                    component: () => import(/* webpackChunkName: "contractXQ" */ '../components/pageAdd/contractXQ.vue'),
                    meta: { title: '合同详情' }
                },
                {
                    path: '/contractAdd',
                    component: () => import(/* webpackChunkName: "contractAdd" */ '../components/pageAdd/contractAdd.vue'),
                    meta: { title: '编辑合同' }
                },
                {
                    path: '/contractEidt',
                    component: () => import(/* webpackChunkName: "contractEidt" */ '../components/pageAdd/contractEidt.vue'),
                    meta: { title: '创建合同' }
                },
                {
                    path: '/journalAdd',
                    component: () => import(/* webpackChunkName: "journalAdd" */ '../components/pageAdd/journalAdd.vue'),
                    meta: { title: '创建日志' }
                },
                {
                    path: '/journallist',
                    component: () => import(/* webpackChunkName: "journallist" */ '../components/pageAdd/journallist.vue'),
                    meta: { title: '日志信息列表' }
                },
                {
                    path: '/journalEidt',
                    component: () => import(/* webpackChunkName: "journalEidt" */ '../components/pageAdd/journalEidt.vue'),
                    meta: { title: '编辑日志' }
                },
                {
                    path: '/journalSearch',
                    component: () => import(/* webpackChunkName: "journalSearch" */ '../components/pageAdd/journalSearch.vue'),
                    meta: { title: '编辑日志' }
                },
                {
                    path: '/journalXQ',
                    component: () => import(/* webpackChunkName: "journalXQ" */ '../components/pageAdd/journalXQ.vue'),
                    meta: { title: '日志详情' }
                },
                {
                    path: '/productSearch',
                    component: () => import(/* webpackChunkName: "productSearch" */ '../components/pageAdd/productSearch'),
                    meta: { title: '产品搜索' }
                },
                {
                    path: '/productlist',
                    component: () => import(/* webpackChunkName: "productlist" */ '../components/pageAdd/productlist'),
                    meta: { title: '产品列表' }
                },
                {
                    path: '/appointmentlist',
                    component: () => import(/* webpackChunkName: "appointmentlist" */ '../components/pageAdd/appointmentlist'),
                    meta: { title: '预约信息列表' }
                },
                {
                    path: '/dadalist',
                    component: () => import(/* webpackChunkName: "dadalist" */ '../components/pageAdd/dadalist'),
                    meta: { title: '资料信息列表' }
                },
                {
                    path: '/userslist',
                    component: () => import(/* webpackChunkName: "userslist" */ '../components/pageAdd/userslist'),
                    meta: { title: '资料信息列表' }
                },
                {
                    path: '/productXQ',
                    component: () => import(/* webpackChunkName: "productXQ */ '../components/pageAdd/productXQ.vue'),
                    meta: { title: '产品详情' }
                },
                {
                    path: '/tasklist',
                    component: () => import(/* webpackChunkName: "tasklist */ '../components/pageAdd/tasklist.vue'),
                    meta: { title: '回复任务清单' }
                },
                {
                    path: '/taskXQ',
                    component: () => import(/* webpackChunkName: "taskXQ */ '../components/pageAdd/taskXQ.vue'),
                    meta: { title: '任务详情' }
                },
                {
                    path: '/taskSearch',
                    component: () => import(/* webpackChunkName: "taskSearch */ '../components/pageAdd/taskSearch.vue'),
                    meta: { title: '任务搜索' }
                },
                {
                    path: '/taskAdd',
                    component: () => import(/* webpackChunkName: "taskAdd */ '../components/pageAdd/taskAdd.vue'),
                    meta: { title: '回复任务清单' }
                },
                {
                    path: '/dataAdd',
                    component: () => import(/* webpackChunkName: "dataAdd */ '../components/pageAdd/dataAdd.vue'),
                    meta: { title: '创建资料' }
                },
                {
                    path: '/datadite',
                    component: () => import(/* webpackChunkName: "datadite */ '../components/pageAdd/datadite.vue'),
                    meta: { title: '编辑整理' }
                },
                {
                    path: '/dataXQ',
                    component: () => import(/* webpackChunkName: "dataXQ */ '../components/pageAdd/dataXQ.vue'),
                    meta: { title: '资料详情' }
                },
                {
                    path: '/dataSearch',
                    component: () => import(/* webpackChunkName: "dataSearch */ '../components/pageAdd/dataSearch.vue'),
                    meta: { title: '资料搜索' }
                },
                {
                    path: '/userAdd',
                    component: () => import(/* webpackChunkName: "userAdd */ '../components/pageAdd/userAdd.vue'),
                    meta: { title: '创建用户' }
                },
                {
                    path: '/userSQ',
                    component: () => import(/* webpackChunkName: "userSQ*/ '../components/pageAdd/userSQ.vue'),
                    meta: { title: '用户授权' }
                },
                {
                    path: '/userdite',
                    component: () => import(/* webpackChunkName: "userdite */ '../components/pageAdd/userdite.vue'),
                    meta: { title: '编辑用户' }
                },
                {
                    path: '/userXQ',
                    component: () => import(/* webpackChunkName: "userXQ */ '../components/pageAdd/userXQ.vue'),
                    meta: { title: '用户资料详情' }
                },
                {
                    path: '/userSearch',
                    component: () => import(/* webpackChunkName: "userSearch */ '../components/pageAdd/userSearch.vue'),
                    meta: { title: '用户搜索' }
                },
                {
                    path: '/dadada',
                    component: () => import(/* webpackChunkName: "dadada */ '../components/pageAdd/dadada.vue'),
                    meta: { title: '搜索系统日志' }
                },
                {
                    path: '/stafflist',
                    component: () => import(/* webpackChunkName: "stafflist */ '../components/page/stafflist.vue'),
                    meta: { title: '创建员工' }
                },
                {
                    path: '/staffAdd',
                    component: () => import(/* webpackChunkName: "staffAdd */ '../components/page/staffAdd.vue'),
                    meta: { title: '创建员工' }
                },
                {
                    path: '/staffedit',
                    component: () => import(/* webpackChunkName: "staffedit */ '../components/page/staffedit.vue'),
                    meta: { title: '编辑员工' }
                },
                {
                    path: '/FGGL',
                    component: () => import(/* webpackChunkName: "FGGL */ '../components/page/FGGL.vue'),
                    meta: { title: '分包管理' }
                },
                {
                    path: '/staffXQ',
                    component: () => import(/* webpackChunkName: "staffXQ */ '../components/page/staffXQ.vue'),
                    meta: { title: '分包管理' }
                },
                {
                    path: '/FGsearch',
                    component: () => import(/* webpackChunkName: "FGsearch */ '../components/page/FGsearch.vue'),
                    meta: { title: '搜索分包' }
                },
                {
                    path: '/staffsearch',
                    component: () => import(/* webpackChunkName: "staffsearch */ '../components/page/staffsearch.vue'),
                    meta: { title: '搜索员工' }
                },
                {
                    path: '/appointmentAdd',
                    component: () => import(/* webpackChunkName: "appointmentAdd */ '../components/page/appointmentAdd.vue'),
                    meta: { title: '创建预约' }
                },
                {
                    path: '/appointmentedit',
                    component: () => import(/* webpackChunkName: "appointmentedit */ '../components/page/appointmentedit.vue'),
                    meta: { title: '编辑预约' }
                },
                {
                    path: '/appointmentXQ',
                    component: () => import(/* webpackChunkName: "appointmentXQ */ '../components/page/appointmentXQ.vue'),
                    meta: { title: '预约详情' }
                },
                {
                    path: '/appointmentsearch',
                    component: () => import(/* webpackChunkName: "appointmentsearch */ '../components/page/appointmentsearch.vue'),
                    meta: { title: '预约详情' }
                },
                {
                    path: '/productAdd',
                    component: () => import(/* webpackChunkName: "productAdd */ '../components/pageAdd/productAdd.vue'),
                    meta: { title: '创建产品' }
                },
                {
                    path: '/productEidt',
                    component: () => import(/* webpackChunkName: "productEidt */ '../components/pageAdd/productEidt.vue'),
                    meta: { title: '编辑产品' }
                },
                {
                    path: '/dadadalist',
                    component: () => import(/* webpackChunkName: "dadadalist */ '../components/page/dadadalist.vue'),
                    meta: { title: '系统日志列表' }
                },
                {
                    path: '/userlist',
                    component: () => import(/* webpackChunkName: "userlist */ '../components/page/userlist.vue'),
                    meta: { title: '用户列表' }
                },
                {
                    path: '/salesAdd',
                    component: () => import(/* webpackChunkName: "salesAdd */ '../components/page/salesAdd.vue'),
                    meta: { title: '创建售后日志' }
                },
                {
                    path: '/saleslist',
                    component: () => import(/* webpackChunkName: "saleslist */ '../components/page/saleslist.vue'),
                    meta: { title: '售后日志列表' }
                },
                {
                    path: '/salesedit',
                    component: () => import(/* webpackChunkName: "salesedit */ '../components/page/salesedit.vue'),
                    meta: { title: '编辑售后日志' }
                },
                {
                    path: '/salesXQ',
                    component: () => import(/* webpackChunkName: "salesXQ */ '../components/page/salesXQ.vue'),
                    meta: { title: '编辑售后日志' }
                },
                {
                    path: '/salessearch',
                    component: () => import(/* webpackChunkName: "salessearch */ '../components/page/salessearch.vue'),
                    meta: { title: '编辑售后日志' }
                },


            ]
        },
        {
            path: '/login',
            component: () => import(/* webpackChunkName: "login" */ '../components/page/Login.vue'),
            meta: { title: '登录' }
        },
        {
            path: '*',
            redirect: '/403'
        },
        {
            path: '*/QX',
            redirect: '/userSQ'
        },
        {
            path: '*/da',
            redirect: '/dadadalist'
        },
        {
            path: '*/ulist',
            redirect: '/userlist'
        }
    ]
});
