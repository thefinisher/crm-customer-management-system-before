module.exports = {
    baseUrl: './',
    assetsDir: 'static',
    productionSourceMap: false,
    // devServer: {
    //     proxy: {
    //         '/api':{
    //             target:'http://jsonplaceholder.typicode.com',
    //             changeOrigin:true,
    //             pathRewrite:{
    //                 '/api':''
    //             }
    //         }
    //     }
    // }
},
module.exports = { // 导出当前模块
    // 配置当前服务的对象
    devServer:{
        open:true, // 默认打开浏览器
        host:'localhost', // 当前项目服务的主机名
        port:8080,  // 端口号
        proxy:{ // 代理端口/代理服务器的对象
            '/crm':{
                target:'http://192.168.11.154:8080',  // 代理的服务器域名
                changeOrigin:true
            }
        }
    }
}